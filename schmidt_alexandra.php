<?php
$img = imagecreatetruecolor(300, 500);

$white = imagecolorallocate($img, 255, 255, 255);
$brown = imagecolorallocate($img, 100, 60, 0);
$black = imagecolorallocate($img, 0, 0, 0);
$orange  = imagecolorallocate($img, 255, 155, 0);
$red  = imagecolorallocate($img, 139,0,0);
$blue  = imagecolorallocate($img, 135,206,235);

imagefill($img, 0, 0, $blue);

imagefilledarc($img, 150, 120, 200, 200,  0, 360, $white, IMG_ARC_EDGED);
imagefilledarc($img, 150, 300, 300, 300,  0, 360, $white, IMG_ARC_EDGED);

imagefilledrectangle($img, 50, 200, 250, 170, $red);
imagefilledrectangle($img, 60, 200, 70, 170, $orange);
imagefilledrectangle($img, 120, 200, 140, 170, $orange);
imagefilledrectangle($img, 180, 200, 200, 170, $orange);
imagefilledrectangle($img, 220, 200, 230, 170, $orange);

imagefilledarc($img, 150, 250, 20, 20, 0, 360, $black, IMG_ARC_EDGED);
imagefilledarc($img, 150, 300, 20, 20, 0, 360, $black, IMG_ARC_EDGED);
imagefilledarc($img, 150, 350, 20, 20, 0, 360, $black, IMG_ARC_EDGED);

imagefilledarc($img,  120,  75,  30,  30,  0, 360, $black, IMG_ARC_EDGED);
imagefilledarc($img,  125,  75,  10,  10,  0, 360, $white, IMG_ARC_EDGED);
imagefilledarc($img,  115,  75,  5,  5,  0, 360, $white, IMG_ARC_EDGED);
imagefilledarc($img,  180,  75,  30,  30,  0, 360, $black, IMG_ARC_EDGED);
imagefilledarc($img,  185,  75,  10,  10,  0, 360, $white, IMG_ARC_EDGED);
imagefilledarc($img,  175,  75,  5,  5,  0, 360, $white, IMG_ARC_EDGED);

imagefilledarc($img,  180,  120,  100,  90,  200, 230, $orange, IMG_ARC_PIE);

imagesetthickness($img, 10);
imagearc($img, 150, 100, 90, 100, 25, 155, $brown);

header("Content-type: image/png");
imagepng($img);

imagedestroy($img);

?>
